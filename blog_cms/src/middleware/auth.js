import { checkLogin } from "../stores/modules/authStore";


export async function requireAuth(to, from, next) {
  
  const isLoggedIn = localStorage.getItem("KEY_USER_STORAGE") != null;

  if (!isLoggedIn) {
    // Nếu không đăng nhập, chuyển hướng đến trang đăng nhập
    next("/auth");
  } else {
    const a =  await checkLogin(localStorage.getItem("KEY_USER_STORAGE"));
    console.log(3432423,a)
     if(a.data == true){
      next("/auth");
     }else{
      next();
     }
  }
}