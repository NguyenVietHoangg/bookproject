import common from "./commonService";
import authorization from "./authorizationService";
import home from "./homeService";
import product from "./productService";
import category from "./categoryService";
import collection from "./collectionService";
import media from "./mediaService";
import setting from "./settingService";
import auth from "./authService";
import user from "./userService";
import menu from "./menuService";
import permission from "./permissionService"
import order from "./orderService"
import discountCode from "./discountCodeService"


export default {
  commonService: common,
  authorizationService: authorization,
  homeService: home,
  productService: product,
  categoryService: category,
  collectionService: collection,
  mediaService: media,
  settingService: setting,
  authService: auth,
  menuService: menu,
  permissionService: permission,
  orderService: order,
  userService: user,
  discountCodeService:discountCode

};
