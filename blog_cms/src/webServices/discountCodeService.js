import { connectServer, discountCodeResource } from "./resources";

export const getDiscountCode = async (data = {}, opts = {}) => {
 
  let {method, url} = discountCodeResource.GET_DISCOUNT_CODE_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};
export const updateDiscountCode = async (data = {}, opts = {}) => {
 
  let {method, url} = discountCodeResource.UPDATE_DISCOUNT_CODE_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};
export const createDiscountCode = async (data = {}, opts = {}) => {
 
  let {method, url} = discountCodeResource.CREATE_DISCOUNT_CODE_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};
export const checkDiscountCode = async (data = {}, opts = {}) => {
 
  let {method, url} = discountCodeResource.CHECK_DISCOUNT_CODE_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};


export default {
  getDiscountCode,
  updateDiscountCode,
  createDiscountCode,
  checkDiscountCode
};
