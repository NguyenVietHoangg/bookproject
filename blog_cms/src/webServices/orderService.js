import { connectServer, orderResource } from "./resources";

export const getOrder = async (data = {}, opts = {}) => {
 
  let {method, url} = orderResource.GET_LIST_ORDER_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};
export const getProducts = async (data = {}, opts = {}) => {
 
  let {method, url} = orderResource.GET_LIST_PRODUCT_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};
export const setDataOrder = async (data = {}, opts = {}) => {
 
  let {method, url} = orderResource.SET_DATA_ORDER_API
  url = `${url}`
  return await connectServer[method](url, {data: data}, opts)
};

export default {
  getOrder,
  getProducts,
  setDataOrder
};
