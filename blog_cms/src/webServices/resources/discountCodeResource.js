export default {

  GET_DISCOUNT_CODE_API: {
    method: "post",
    url: "/cms/discountcode",
  },
  UPDATE_DISCOUNT_CODE_API: {
    method: "post",
    url: "/cms/discountcode/update",
  },
  CREATE_DISCOUNT_CODE_API: {
    method: "post",
    url: "/cms/discountcode/create",
  },
  CHECK_DISCOUNT_CODE_API : {
    method: "post",
    url: "/cms/discountcode/check",
  }
};
