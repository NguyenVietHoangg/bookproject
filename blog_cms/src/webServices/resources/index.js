import server from "../../configs/connectServer";
import common from "./commonResource";
import product from "./productResource";
import category from "./categoryResource";
import collection from "./collectionResource";
import media from "./mediaResource";
import setting from "./settingResource";
import auth from "./authResource";
import menu from "./menuResource"
import permission from "./permissionResource"
import order from "./orderResource"
import user from "./userResource"
import discountCode from "./discountCodeResource"

export const connectServer = server;
export const commonResource = common;
export const productResource = product;
export const categoryResource = category;
export const collectionResource = collection;
export const mediaResource = media;
export const settingResource = setting;
export const authResource = auth;
export const menuResource = menu;
export const permissionResource = permission;
export const orderResource = order;
export const userResource = user;
export const discountCodeResource = discountCode;

export default {
  connectServer,
  commonResource,
  productResource,
  categoryResource,
  collectionResource,
  settingResource,
  authResource,
  menuResource,
  permissionResource,
  orderResource,
  userResource,
  discountCodeResource
};
