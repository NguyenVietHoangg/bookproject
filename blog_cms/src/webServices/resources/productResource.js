export default {
  //product
  GET_PRODUCT_LIST_API: {
    method: 'post',
    url: '/cms/story/getStoryList'
  },
  GET_PRODUCT_DETAIL_BY_ID_API: {
    method: 'get',
    url: '/cms/story/getStoryDetailById'
  },
  SAVE_PRODUCT_DETAIL_BY_ID_API: {
    method: 'post',
    url: '/cms/story/saveStoryDetailById'
  },
  SREACH_STORY_API:{
    method: 'post',
    url: '/cms/common/sreachstory'
  }
}