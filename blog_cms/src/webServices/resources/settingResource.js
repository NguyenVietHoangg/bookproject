export default {
  //setting
  GET_SETTING_APP_INFO: {
    method: 'get',
    url: '/cms/setting/getSettingAppInfo'
  },
  SAVE_SETTING_APP_INFO: {
    method: 'post',
    url: '/cms/setting/saveSettingAppInfo'
  },
}