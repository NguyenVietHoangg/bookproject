export default {
  //product
  GET_LOGIN_API: {
    method: "post",
    url: "/login",
  },
  GET_LOGOUT_API: {
    method: "post",
    url: "/logout",
  },
  CHECK_LOGIN_API: {
    method: "post",
    url: "/checklogin",
  },
  GET_LIST_USER_API: {
    method: "post",
    url: "/listuser",
  },
};
