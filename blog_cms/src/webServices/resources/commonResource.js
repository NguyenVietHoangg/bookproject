export default {
  //common
  GET_COMMON_REFS_API: {
    method: 'post',
    url: '/cms/common/getCommonRefs'
  },
  POST_CHANGE_STATUS_API: {
    method: 'post',
    url: '/cms/common/postChangeStatus'
  }
}