export default {
  //product
  GET_COLLECTION_LIST_API: {
    method: 'post',
    url: '/cms/collection/getCollectionList'
  },
  GET_COLLECTION_DETAIL_BY_ID_API: {
    method: 'get',
    url: '/cms/collection/getCollectionDetailById'
  },
  GET_COLLECTION_PARENT_LIST_API: {
    method: 'get',
    url: '/cms/collection/getCollectionParentList'
  },
  SAVE_COLLECTION_DETAIL_BY_ID_API: {
    method: 'post',
    url: '/cms/collection/saveCollectionDetailById'
  }
}