import {
  defineStore
} from 'pinia';
import { USER_STORE } from './../../configs/storeTypes'
import { removeUserStoreStore } from './../../configs/connectServer'
import wsv from "../../webServices";

export const useUserStore = defineStore(USER_STORE, {
  state: () => ({
    user : {
    }
  }),
  actions: {
   
    async setOrder(data = {}) {
      const res = await wsv.orderService.setDataOrder(data);
      this.dbProduct = res;
    },  
    logout() {
      removeUserStoreStore()
      this.user = {}
    }
  }
})

export default {
  useUserStore
}
