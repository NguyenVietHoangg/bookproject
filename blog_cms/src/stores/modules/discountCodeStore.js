import { defineStore } from "pinia";
import { _dataTable, _showAlert } from "../../configs/globalProperties";
import wsv from "../../webServices";
/**
 * lưu trữ Danh sách hiện tại
 */
export const discountCodeStore = defineStore("discountCode", {
  state: () => ({
    db: { a: "a" },
  }),
  actions: {
    async getDiscountCodeSList() {
      const res = await wsv.discountCodeService.getDiscountCode();
      this.db = res;
    },   
    async updateDiscountCodeSList(data={}) {
      const res = await wsv.discountCodeService.updateDiscountCode(data);
      this.db = res;
    },  
    
    async createDiscountCodeSList(data={}) {
      const res = await wsv.discountCodeService.createDiscountCode(data);
      this.db = res;
    },  
    
    async checkDiscountCode(data={}) {
      const res = await wsv.discountCodeService.checkDiscountCode(data);
      this.db = res;
    }, 
  },
});
export default {
    discountCodeStore,
};
