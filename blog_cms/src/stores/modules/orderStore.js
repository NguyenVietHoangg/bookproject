import { defineStore } from "pinia";
import { _dataTable, _showAlert } from "../../configs/globalProperties";
import wsv from "../../webServices";
/**
 * lưu trữ Danh sách hiện tại
 */
export const orderStore = defineStore("order", {
  state: () => ({
    db: { a: "a" },
    dbProduct:{}
  }),
  actions: {
    async getOrderList(data = {}) {
      const res = await wsv.orderService.getOrder();
      this.db = res;
    },
    async getProductList(data = {}) {
      const res = await wsv.orderService.getProducts(data);
      this.dbProduct = res;
    }, 
    async setOrder(data = {}) {
      const res = await wsv.orderService.setDataOrder(data);
      this.dbProduct = res;
    },  
    
  },
});

export default {
  orderStore,
};
