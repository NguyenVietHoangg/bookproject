import useHomeStore from "./modules/homeStore";
import useUserStore from "./modules/userStore";
import useSystemStore from "./modules/systemStore";
import useProductStore from "./modules/productStore";
import useCategoryStore from "./modules/categoryStore";
import useCollectionStore from "./modules/collectionStore";
import useMediaStore from "./modules/mediaStore";
import settingStore from "./modules/settingStore";
import menuStore from "./modules/menuStore"
import permissionStore from "./modules/permissionStore"
import orderStore from "./modules/orderStore"
import discountCodeStore from "./modules/discountCodeStore"



export default {
  useHomeStore,
  useUserStore,
  useSystemStore,
  useProductStore,
  useCategoryStore,
  useCollectionStore,
  useMediaStore,
  settingStore,
  menuStore,
  permissionStore,
  orderStore,
  discountCodeStore
  
}