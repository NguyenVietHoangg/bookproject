/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './app.vue',
    './error.vue',
  ],
  theme: {
    extend: {
      colors: {
        'chan-trang': '#001D74',
        xanh: '#005FD0',
        do: '#DA251C',
        white: '#ffffff',
        text1: '#141933',
        text2: '#505673',
        text3: '#878CA8',
        back1: '#DADEF2',
        back: '#F1F1F1',
        'back-lighter': '#F2F3F5',
        'back-lightest': '#FAFAFA',
        'check-ok': '#1D8C64',
        cam: '#F3A127',
        warning: '#C83131',
        icon: '#9DA6BA',
      },
    },
    fontSize: {
      sm: ['12px', '18px'],
      base: ['14px', '20px'],
      lg: ['16px', '24px'],
      xl: ['20px', '28px'],
      '2xl': ['24px', '32px'],
      md: ['18px', '27px'],
    },
  },
  plugins: [],
};
