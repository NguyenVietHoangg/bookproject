// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxt/ui',
    '@vueuse/nuxt',
    '@pinia/nuxt', // https://go.nuxtjs.dev/axios
    // https://go.nuxtjs.dev/pwa
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    'nuxt-swiper',

    //'@nuxtjs/tailwindcss',
  ],
  css: ['@fortawesome/fontawesome-free/css/all.css', './assets/css/reset.css'],
  ssr: false,
  // postcss: {
  //     plugins: {
  //         tailwindcss: './tailwind.config.js',
  //         autoprefixer: {},
  //     },
  // },
});
