import { defineStore } from 'pinia';

/**
 * lưu trữ Danh sách hiện tại
 */
export const useSettingStore = defineStore('setting', {
  state: () => ({
    header: {
      color: '',
    },
    footer: {
      direction: 'w-full flex flex-row gap-3',
      styleItem:
        ' py-[8px] w-full flex items-center gap-3  text-lg font-bold text-text1  hover:text-xanh',
      bg: '',
      subSetting: {
        direction: ' w-full flex flex-col gap-3',
        styleItem:
          ' py-[8px] w-full flex items-center gap-3  text-base text-text1 ',
        bg: '',
        sizeImg: '',
      },
    },
    navbar: {
      direction: 'w-full flex flex-col gap-3',
      styleItem:
        ' py-[8px] w-full flex items-center gap-3  text-text1 hover:bg-red-500 hover:text-white',
      bg: '',
      sizeImg: '',
      subSetting: {},
    },
  }),
  actions: {
    asdasd() {
      return 'sdfsdfsdfd';
    },
  },
});

export default {
  useSettingStore,
};
