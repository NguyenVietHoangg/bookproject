'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ordered_products', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      orderId: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      storyId: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      number: {
        type: Sequelize.INTEGER,
        defaultValue: 2
      },
      content: {
        type: Sequelize.STRING,
        defaultValue: null,
        allowNull: true
      },
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 2
      },
      type: {
        type: Sequelize.INTEGER,
        defaultValue: 2
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    }, {
      uniqueKeys: [],
    }).then(() => {

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ordered_products');
  }
};