'use strict';
module.exports = (Sequelize, DataTypes) => {
  const order = Sequelize.define('order', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    discountCode: {
      type: DataTypes.STRING,
      defaultValue: null,
      allowNull: true
    },
    note: {
      type: DataTypes.STRING,
      defaultValue: null,
      allowNull: true
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    updatedBy: {
      allowNull: true,
      type: DataTypes.INTEGER,
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {});
  
  order.associate = function(models) {
    // associations can be defined here
   
  };
  return order;
};