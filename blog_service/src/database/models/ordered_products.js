'use strict';
module.exports = (Sequelize, DataTypes) => {
  const ordered_products = Sequelize.define('ordered_products', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    orderId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    storyId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    number: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    content: {
      type: DataTypes.STRING,
      defaultValue: null,
      allowNull: true
    },
    type: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {});
  ordered_products.associate = function(models) {
    // associations can be defined here

  };
  return ordered_products;
};