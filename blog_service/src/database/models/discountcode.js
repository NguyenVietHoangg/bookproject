'use strict';
module.exports = (Sequelize, DataTypes) => {
  const discountcode = Sequelize.define('discountcode', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: null,
      unique: true
    },
    discountAmount: {
      type: DataTypes.INTEGER,
    },
    unit: {
      type: DataTypes.ENUM("%", "pice"),
      defaultValue: null,
      allowNull: true
    },
    criteria: {
      type: DataTypes.INTEGER,
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 10
    },
    startDay: {
      type: DataTypes.DATE
    },
    endDay: {
      type: DataTypes.DATE
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    updatedBy: {
      allowNull: true,
      type: DataTypes.INTEGER,
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {});
  discountcode.associate = function(models) {
    // associations can be defined here
  };
  return discountcode;
};