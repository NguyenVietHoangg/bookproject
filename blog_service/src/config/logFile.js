import winston from 'winston'

const PATH_INFO = `${__dirname}/../logs/infoLog`
const PATH_ERROR = `${__dirname}/../logs/errorLog`

const options = {
  levels: {
    trace: 0,
    input: 1,
    verbose: 2,
    prompt: 3,
    debug: 4,
    info: 5,
    data: 6,
    help: 7,
    warn: 8,
    error: 9
  },
  colors: {
    trace: 'magenta',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    debug: 'blue',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    error: 'red'
  },
  file: {
    level: 'info',
    name: 'file.info',
    filename: PATH_INFO,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 100,
    colorize: true,
  },
  errorFile: {
    level: 'error',
    name: 'file.error',
    filename: PATH_ERROR,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 100,
    colorize: true,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

// instantiate a new Winston Logger with the settings defined above
// export const logger = winston.createLogger({
//   level: 'error',
//   format: winston.format.combine(winston.format.json(), winston.format.prettyPrint(), winston.format.colorize()),
//   transports: [
//     new (winston.transports.Console)(options.console),
//     new (winston.transports.File)(options.file),
//     new (winston.transports.File)(options.errorFile)
//   ],
//   exitOnError: false, // do not exit on handled exceptions
// });

const myFormatter = winston.format((info) => {
  const {message} = info;

  if (info.data) {
    info.message = `${message} ${JSON.stringify(info.data)}`;
    delete info.data; // We added `data` to the message so we can delete it
  }
  
  return info;
})();

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    winston.format.colorize(),
    myFormatter,
    winston.format.simple(),
  ),
  transports: [
    new (winston.transports.Console)(options.console),
    new (winston.transports.File)(options.file),
    new (winston.transports.File)(options.errorFile)
  ],
  exitOnError: false,
});


// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write: function(message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message);
  },
};


export default logger;