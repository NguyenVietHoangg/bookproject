import express from "express";
import * as _e from "../../config/eResponse";
import user from "./service";

const router = express.Router();

router.post("/login", async (req, res) => {
  const conds = req.body;
  const data = await user.getUserLogin(conds);

  res.status(data.status).json({ code: data.status, message: data.message });
});
router.post("/logout", async (req, res) => {
  const conds = req.body.data;
  const data = await user.getUserLogout(conds);

  res.status(data.status).json({ code: data.status, message: data.message });
});
router.post("/listuser", async (req, res) => {
  console.log(req.body.data);
  const conds = req.body.data;
  const data = await user.getSreachUser(conds);

  res.end(_e._successMenu("okiii", data))
});
router.post("/checklogin", async (req, res) => {
  console.log(123)
  const conds = req.body.data;
  const data = await user.checkLogin(conds);
  res.end(_e._successMenu("ccccc", data))
});


export default router;
