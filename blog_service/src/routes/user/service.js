
import * as Model from "../../database/models";
import { signToken, validToken,decodeToken } from "../../helpers/jwtToken";
const { Op } = require('sequelize');
export const getUserLogin = async (data) => {
  // const { email, password } = data;
  try{
    const email = data.data.email;
    const password = data.data.password;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const user = await Model.user.findOne({ where: { email: email } });
    if (!emailRegex.test(email) || password <= 8) {
      return { status: 500, message: " Data is not in the correct format" };
    }
    if (user == null) {
      return { status: 500, message: " Account or password is incorrect" };
    }
    // Kiểm tra xem người dùng có tồn tại không
  
    const isValidPassword = password === user.password ? true : false;
    if (!isValidPassword) {
      return { status: 500, message: " Account or password is incorrect" };
    }
    //Tạo token JWT
    const token = await signToken({ id: user.id, name: user.name, grouproleid : user.grouproleId });
    return { status: 200, message: token };
  } catch{
    return { status: 500, message: "Cannot connect to server!" };
  }
 
};
export const getUserLogout = async (data) =>  {
  res.clearCookie('');
  return { status: 200, message: "okii" };
}
export const checkLogin = async (data) =>  {
  
  const db =  await decodeToken(data);

  if(db){
    return "okiii" ;
  }
  else{
    return true ;
  }
}

export const getSreachUser = async (data) =>  {
  const listInfoUser = await Model.user.findAll({
    where: {
      [Op.or]: [
        { email: { [Op.like]: `%${data}%` } },
        { fullname: { [Op.like]: `%${data}%` } },
        { phone: { [Op.like]: `%${data}%` } },
      ]
    },
    attributes: ['id','name', 'fullname', 'email', 'address', 'phone']
  });
  return listInfoUser;
}
export default {
  getUserLogin,
  getUserLogout,
  checkLogin,
  getSreachUser
};
