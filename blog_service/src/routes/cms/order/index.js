import express from 'express'
import * as _e from '../../../config/eResponse'
import { ADD_SUCCESS, UPDATE_SUCCESS, SUCCESS, ERROR, NOT_FOUND } from '../../../config/typeAlias'
import * as order from './service'
import {decodeToken} from '../../../helpers/jwtToken'

const router = express.Router()
router.post(`/listorder`, async (req, res) =>{
    const data = await order.getListOrder()
    res.end(_e._successMenu(SUCCESS, data))
  })

  router.post(`/liststory`, async (req, res) =>{
    console.log(req.body)
    const data = await order.getListStory(req.body.data)
    res.end(_e._successMenu(SUCCESS, data))

  })
// router.get(`/order/:id`, async (req, res) =>{
//   const id = req.params.id
//     const data = await menu.getListStory(groupRoleID)
//     res.end(_e._successMenu(SUCCESS, data))
//   })
  
  export default router