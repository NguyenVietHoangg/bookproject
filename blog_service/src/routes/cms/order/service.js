import _, { isInteger } from 'lodash'
import { LIMIT } from '../../../config/common'
import { genSlug } from '../../../utils/stringHelpers'
import * as Model from '../../../database/models'
import * as _e from '../../../config/eResponse'
const { Op } = Model.sequelize



export const getListOrder = async () => {
    console.log(1231)
    Model.order.belongsTo(Model.user, {
        foreignKey: "userid",
      });
    
    const infoOrder = await Model.order.findAll({
        include: [{
            model: Model.user,
            attributes: ['name','fullname','email','address','phone']
        }],
    });
    return infoOrder;
};

export const getListStory = async (id) => {
    Model.ordered_products.belongsTo(Model.story, {
        foreignKey: "storyId",
      });
      const orderedProducts = await Model.ordered_products.findAll({
            include: [{
                model: Model.story
            }],
            where: {
                orderId : id
            }
        });
    return orderedProducts;
};


