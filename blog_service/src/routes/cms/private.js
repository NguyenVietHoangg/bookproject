import common from "./common";
import media from "./media";
import setting from "./setting";
import category from "./category";
import collection from "./collection";
import story from "./story";
import menu from "./menu"
import order from "./order"
import discountcode from "./discountcode"


const router = [common, media, setting, category, collection, story, menu, order,discountcode];
export default router;
