import express from 'express'
import * as _e from '../../../config/eResponse'
import { ADD_SUCCESS, UPDATE_SUCCESS, SUCCESS,UPDATE_FAILED, ERROR, NOT_FOUND } from '../../../config/typeAlias'
import * as discountCode from './service'

const router = express.Router()
router.post(`/discountcode`, async (req, res) =>{
    const data = await discountCode.getListDiscountCode();
    res.end(_e._successMenu(SUCCESS, data))
  })
  router.post(`/discountcode/update`, async (req, res) =>{
    const data = await discountCode.updateListDiscountCode(req.body.data);
    if(data === UPDATE_SUCCESS){

      res.end(_e._successDiscountCode(data))
    } 
    res.end(_e._errorDiscountCode(data))
  })
  router.post(`/discountcode/create`, async (req, res) =>{
    const data = await discountCode.createListDiscountCode(req.body.data);
    if(typeof data === 'object'){
      res.end(_e._successDiscountCode(ADD_SUCCESS,data))
    }
    res.end(_e._errorDiscountCode(data))
  })
  router.post(`/discountcode/check`, async (req, res) =>{
    const data = await discountCode.checkDiscountCode(req.body.data);
    if (typeof data === 'object') {
      res.end(_e._successDiscountCode(ADD_SUCCESS, data));
    } else {
      res.end(_e._errorDiscountCode(data));
    }
  })


  export default router