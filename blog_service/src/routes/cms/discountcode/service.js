import * as Model from "../../../database/models";
import * as _e from "../../../config/eResponse";
import {
  ADD_SUCCESS,
  START_DAY_ERROR,
  END_DAY_ERROR,
  DISCONST_AMOUNT_ERROR,
  CRITERIA_ERROR,
  UPDATE_SUCCESS,
  ERROR,
  NAME_CODE_ERROR,
  VALUE_REQUEST_IS_EMPTY,
  NO_CODE_ERROR,
  OUT_DATE_ERROR
} from "../../../config/typeAlias";
const { Op } = Model.sequelize;

export const getListDiscountCode = async () => {
  const ListDiscountCode = await Model.discountcode.findAll();
  return ListDiscountCode;
};

export const updateListDiscountCode = async (data) => {
  try {
    const checkValidate = await validateDiscountCode(data);
    if (checkValidate == "oki") {
      const checkId = await Model.discountcode.update(data, {
        where: {
          id: data.id,
        },
      });
      if (checkId) {
        const a = await Model.discountcode.update(data, {
          where: {
            id: data.id,
          },
        });
        return UPDATE_SUCCESS;
      }
    } else {
      return checkValidate;
    }
  } catch (error) {
    console.error("Error updating record:", error);
    return ERROR;
  }
};
export const createListDiscountCode = async (data) => {
  try {
    const checkValidate = await validateDiscountCode(data);
    if (checkValidate == "oki") {
      await Model.discountcode.create(data);
      const createdCode = await Model.discountcode.findOne({
        where: { name: data.name },
      });
      return createdCode;
    } else {
      return checkValidate;
    }
  } catch (error) {
    console.error("Error updating record:", error);
    return ERROR;
  }
};

export const checkDiscountCode = async (data) => {
  try {
    console.log(123)
    const discountCode = await Model.discountcode.findOne({
      where: { name: data },
    });
    if (discountCode) {
      const currentDay = new Date().getTime();
      const startDay = new Date(discountCode.startDay).getTime();
      const endDay = new Date(discountCode.endDay).getTime();
      if (currentDay >= startDay && currentDay <= endDay ) {
        return discountCode;
      }
      return OUT_DATE_ERROR;
      
    } else {
      return NO_CODE_ERROR;
    }
  } catch (error) {
    console.error("Error updating record:", error);
    return ERROR;
  }
};
const validateDiscountCode = async (data) => {
  if (
    !data.name ||
    !data.startDay ||
    !data.endDay ||
    !data.discountAmount ||
    !data.criteria
  ) {
    return VALUE_REQUEST_IS_EMPTY;
  }
  const currentDay = new Date();
  const startDay = new Date(data.startDay);
  const endDay = new Date(data.endDay);
  if (parseInt(data.discountAmount) < 0) {
    return DISCONST_AMOUNT_ERROR;
  }
  if (parseInt(data.criteria) < 0) {
    return CRITERIA_ERROR;
  }
  if (startDay.getTime() < currentDay.getTime()) {
    return START_DAY_ERROR;
  }
  if (startDay.getTime() > endDay.getTime()) {
    return END_DAY_ERROR;
  }
  const checkData = await Model.discountcode.findOne({
    where: {
      name: data.name,
    },
  });
  if (checkData) {
    return NAME_CODE_ERROR;
  }
  return "oki";
};
